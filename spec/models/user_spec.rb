require 'rails_helper'

RSpec.describe User, :type => :model do
  describe 'associations' do 
  	it { is_expected.to have_one :address }
  	it { is_expected.to have_many :orders }
  	it { is_expected.to have_many :line_items }
  end 

  describe 'validations' do 
  	it { is_expected.to validate_presence_of :first_name }
  	it { is_expected.to validate_presence_of :last_name }
  	it { is_expected.to validate_presence_of :role }
  end
  describe 'delegations' do 
  end
end
