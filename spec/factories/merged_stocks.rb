FactoryGirl.define do
  factory :merged_stock do
    stock nil
    quantity "9.99"
    date "2016-10-03"
  end
end
