class AddDiscountedToStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :stocks, :discounted, :boolean, default: false
    add_column :stocks, :discount_amount, :decimal, default: 0
  end
end
