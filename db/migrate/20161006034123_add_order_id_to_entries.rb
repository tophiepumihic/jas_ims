class AddOrderIdToEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :order_id, :integer
  end
end
