class AddMergedToStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :stocks, :merged, :boolean, default: false
  end
end
