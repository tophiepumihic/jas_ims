class AddMemberTypeToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :member_type, :integer
    add_index :users, :member_type
  end
end
