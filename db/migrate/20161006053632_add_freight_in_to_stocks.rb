class AddFreightInToStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :stocks, :has_freight, :boolean, default: false
    add_column :stocks, :freight_amount, :decimal, default: 0
  end
end
