class AddMergedStockIdToMergedStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :merged_stocks, :merged_stock_id, :integer
    add_index :merged_stocks, :merged_stock_id
  end
end
