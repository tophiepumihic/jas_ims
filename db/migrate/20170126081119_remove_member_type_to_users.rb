class RemoveMemberTypeToUsers < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users, :member_type, :integer
  end
end
