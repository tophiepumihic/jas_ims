class CreateInterestRates < ActiveRecord::Migration[5.0]
  def change
    create_table :interest_rates do |t|
      t.decimal :rate

      t.timestamps
    end
  end
end
