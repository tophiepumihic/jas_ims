class CreateMergedStocks < ActiveRecord::Migration[5.0]
  def change
    create_table :merged_stocks do |t|
      t.belongs_to :stock, foreign_key: true
      t.decimal :quantity
      t.date :date

      t.timestamps
    end
  end
end
