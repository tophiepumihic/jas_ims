class AddTypeToInterestRates < ActiveRecord::Migration[5.0]
  def change
    add_column :interest_rates, :type, :string
    add_index :interest_rates, :type
  end
end
