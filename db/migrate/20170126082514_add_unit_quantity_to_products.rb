class AddUnitQuantityToProducts < ActiveRecord::Migration[5.0]
  def change
  	add_column :products, :unit_quantity, :integer
  end
end
