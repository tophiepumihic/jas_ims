class Stock < ApplicationRecord
  include PgSearch
  pg_search_scope :search_by_name, :against => [:name, :serial_number], :associated_against => {
    :product => [:name_and_description]}
  enum stock_type:[:purchased, :returned, :discontinued]
  enum payment_type: [:cash, :credit]
  belongs_to :product
  belongs_to :employee
  belongs_to :supplier
  has_one :entry, class_name: "Accounting::Entry", foreign_key: 'stock_id', dependent: :destroy
  has_many :line_items
  has_many :orders, through: :line_items
  has_many :refunds
  has_many :merged_stocks, foreign_key: 'merged_stock_id', dependent: :destroy,  class_name: "MergedStock"
  has_many :stocks_merged, class_name: "MergedStock", foreign_key: 'stock_id',  dependent: :destroy
  before_update :set_prices
  scope :created_between, lambda {|start_date, end_date| where("date >= ? AND date <= ?", start_date, end_date )}

  validates :quantity, :supplier_id, :purchase_price, :unit_price,  presence: true, numericality: true
  before_save :set_date, :set_prices, :set_name
  before_destroy :ensure_no_merged_stocks

  def to_s
    name
  end

  def self.not_merged 
    all.select{ |a| !a.merged?}
  end

  def self.returned
    all.where(stock_type: 1)
  end

  def self.discontinued
    all.where(stock_type: 2)
  end

  def self.total_cost_of_purchase
    all.sum(:purchase_price)
  end

  def self.entered_on(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date])
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date])
      where('date' => from_date..to_date)
    else
      all
    end
  end

  def total_wholesale_quantity
    (quantity/self.product.unit_quantity).to_i
  end

  def total_retail_quantity
    (((quantity/self.product.unit_quantity).modulo(1)) * self.product.unit_quantity).to_i
  end

  def converted_total_quantity
    if (self.product.unit_quantity && self.product.highest_unit).present?
      if total_retail_quantity != 0
        "#{total_wholesale_quantity} #{self.product.highest_unit}/s" + " & " + "#{total_retail_quantity} #{self.product.unit}/s"
      else
        "#{total_wholesale_quantity}  #{self.product.highest_unit}/s"
      end
    else
      "#{quantity} #{self.product.unit}/s"
    end
  end

  def in_stock
    quantity -  sold
  end

  def wholesale_quantity
    (in_stock/self.product.unit_quantity).to_i
  end

  def retail_quantity
    (((in_stock/self.product.unit_quantity).modulo(1)) * self.product.unit_quantity).to_i
  end

  def converted_in_stock_quantity
    if (self.product.unit_quantity && self.product.highest_unit).present?
      if retail_quantity != 0
        "#{wholesale_quantity} #{self.product.highest_unit}/s" + " & " + "#{retail_quantity} #{self.product.unit}/s"
      else
        "#{wholesale_quantity}  #{self.product.highest_unit}/s"
      end
    else
      "#{quantity} #{self.product.unit}/s"
    end
  end

  def sold
    line_items.sum(:quantity)
  end

  def wholesale_sold_quantity
    (sold/self.product.unit_quantity).to_i
  end

  def retail_sold_quantity
    (((sold/self.product.unit_quantity).modulo(1)) * self.product.unit_quantity).to_i
  end

  def converted_sold_quantity
    if (self.product.unit_quantity && self.product.highest_unit).present?
      if retail_sold_quantity != 0
        "#{wholesale_sold_quantity} #{self.product.highest_unit}/s" + " & " + "#{retail_sold_quantity} #{self.product.unit}/s"
      else
        "#{wholesale_sold_quantity}  #{self.product.highest_unit}/s"
      end
    else
      "#{quantity} #{self.product.unit}/s"
    end
  end

  def total_merged_stocks
    stocks_merged.sum(:quantity)
  end

  def purchase_price_less_discount
    purchase_price - discount_amount
  end

  def purchase_price_less_discount_less_freight_in
    (quantity * unit_price) - discount_amount
  end

  def purchase_price_less_freight_in
    purchase_price - freight_amount
  end

  def purchase_price_plus_freight_in
    purchase_price
  end

  def purchase_price_less_discount_plus_freight_in
    purchase_price - discount_amount
  end

  def self.expired
    all.select{ |a| a.expired?}
  end

  def self.out_of_stock
    all.select{ |a| a.out_of_stock?}
  end

  def expired?
    if expiry_date.present?
      Time.zone.now >= expiry_date && !discontinued?
    else
      false
    end
  end

  def expired_and_low_stock?
    expired? && low_stock?
  end

  def discontinued?
    self.stock_type == "discontinued"
  end

  def expired_and_discontinued?
    expired? && discontinued?
  end

  def returned?
    self.stock_type == "returned"
  end

  def out_of_stock?
    in_stock.zero? || in_stock.negative?
  end

  def low_stock?
    quantity <= product.stock_alert_count && !out_of_stock?
  end

  def status
    if out_of_stock?
      "Out of Stock"
    elsif low_stock? && !expired? && !discontinued?
      "Low on Stock"
    elsif expired_and_low_stock? || (expired? && !discontinued?)
      "Expired"
    elsif expired_and_discontinued? || self.discontinued?
      "Discontinued"
    elsif returned?
      "Returned"
    end
  end

  def set_stock_status_to_product
    if !out_of_stock? && !low_stock?
      self.product.available!
    elsif low_stock?
      self.product.low_stock!
    elsif out_of_stock?
      self.product.out_of_stock!
    end
  end

  def in_stock_amount
    in_stock * unit_price
  end

  def create_expense_from_expired_stock
    Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.id, commercial_document_type: self.class, date: Time.zone.now, description: "Expired stock for #{self.product.name_and_description} with quantity of #{self.converted_in_stock_quantity}", debit_amounts_attributes: [amount: self.in_stock_amount, account: "Spoilage, Breakage and Losses (Selling/Marketing Cost)"], credit_amounts_attributes:[amount: self.in_stock_amount, account: 'Merchandise Inventory'],  employee_id: self.employee_id)
  end

  def remove_expense_from_expired_stock
    @entry = Accounting::Entry.where(stock_id: self.id).where(commercial_document_id: self.id).where(commercial_document_type: self.class).where(description: "Expired stock for #{self.product.name_and_description} with quantity of #{self.converted_in_stock_quantity}").last
    if @entry.present?
      @entry.destroy
    else
      false
    end
  end

  def create_entry
    #CASH PURCHASE ENTRY##
    if self.cash? && !discounted? && !has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Cash Purchase of stocks", debit_amounts_attributes: [amount: self.purchase_price, account: "Merchandise Inventory"], credit_amounts_attributes:[amount: self.purchase_price, account: 'Cash on Hand'],  employee_id: self.employee_id)
    elsif self.cash? && discounted? && !has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Cash Purchase of stocks with discount of #{self.discount_amount}", debit_amounts_attributes: [amount: self.purchase_price, account: "Merchandise Inventory"], credit_amounts_attributes:[amount: self.purchase_price, account: 'Cash on Hand'],  employee_id: self.employee_id)
    elsif self.cash? && !discounted? && has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Cash Purchase of stocks with freight in of #{self.freight_amount}", debit_amounts_attributes: [{amount: self.purchase_price_less_freight_in, account: "Merchandise Inventory"}, {amount: self.freight_amount, account: "Freight In"}], credit_amounts_attributes:[{amount: self.purchase_price_less_freight_in, account: 'Cash on Hand'}, {amount: self.freight_amount, account: "Cash on Hand"}],  employee_id: self.employee_id)
    elsif self.cash? && discounted? && has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Cash Purchase of stocks with discount of #{self.discount_amount} and freight in of #{self.freight_amount}", debit_amounts_attributes: [{amount: self.purchase_price_less_discount_less_freight_in, account: "Merchandise Inventory"}, {amount: self.freight_amount, account: "Freight In"}], credit_amounts_attributes:[{amount: self.purchase_price_less_discount_less_freight_in, account: 'Cash on Hand'}, {amount: self.freight_amount, account: "Cash on Hand"}],  employee_id: self.employee_id)
      
       #Cedit PURCHASE ENTRY##
    elsif self.credit? && !discounted? && !has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Credit Purchase of stocks", debit_amounts_attributes: [amount: self.purchase_price, account: "Merchandise Inventory"], credit_amounts_attributes:[amount: self.purchase_price, account: 'Accounts Payable-Trade'],  employee_id: self.employee_id)
    elsif self.credit? && discounted? && !has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Credit Purchase of stocks with discount of #{self.discount_amount}", debit_amounts_attributes: [amount: self.purchase_price, account: "Merchandise Inventory"], credit_amounts_attributes:[amount: self.purchase_price, account: 'Accounts Payable-Trade'],  employee_id: self.employee_id)
    elsif self.credit? && !discounted? && has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Credit Purchase of stocks with freight in of #{self.freight_amount}", debit_amounts_attributes: [{amount: self.purchase_price_less_freight_in, account: "Merchandise Inventory"}, {amount: self.freight_amount, account: "Freight In"}], credit_amounts_attributes:[{amount: self.purchase_price_less_freight_in, account: 'Accounts Payable-Trade'}, {amount: self.freight_amount, account: "Accounts Payable-Trade"}],  employee_id: self.employee_id)
    elsif self.credit? && discounted? && has_freight?
      Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.supplier.id, commercial_document_type: self.supplier.class, date: self.date, description: "Credit Purchase of stocks with discount of #{self.discount_amount} and freight in of #{self.freight_amount}", debit_amounts_attributes: [{amount: self.purchase_price_less_discount_less_freight_in, account: "Merchandise Inventory"}, {amount: self.freight_amount, account: "Freight In"}], credit_amounts_attributes:[{amount: self.purchase_price_less_discount_less_freight_in, account: 'Accounts Payable-Trade'}, {amount: self.freight_amount, account: "Accounts Payable-Trade"}],  employee_id: self.employee_id)
   end
  end

  def merge_stock
    if self.merged?
      MergedStock.create(merged_stock_id: self.id, quantity: self.quantity, date: self.date, stock_id: self.product_to_be_merged_into)
    else 
      false
    end
  end
  def product_to_be_merged_into
    if product.stocks.not_merged.present?
      self.product.stocks.not_merged.first.id
    else
      nil
    end
  end
  def set_to_merged!
    self.merged = true 
    self.save 
  end
  private
  def set_prices
    self.retail_price = self.product.price
    self.wholesale_price = self.product.wholesale_price
  end
  def set_date
    if self.date.nil?
      self.date = Time.zone.now
    end
  end
  def set_name
    self.name = self.product.name
  end
  def ensure_no_merged_stocks 
    return false if self.total_merged_stocks > 0
  end
end
