function calculateTotalPurchase() {
  var quantity = document.getElementById('stock_quantity').value;
  var unitPrice = document.getElementById('stock_unit_price').value;
  var discount = document.getElementById('discount-amount').value;
  var freight_in = document.getElementById('freight-amount').value;

  var totalPurchasePrice = document.getElementById('stock_purchase_price');
  var myResult = quantity * unitPrice + parseFloat(freight_in)- parseFloat(discount);
  totalPurchasePrice.value = myResult;
}
